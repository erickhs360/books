import axios from 'axios';
import { isAuthenticated } from '../auth';

const api = axios.create({
  baseURL: 'https://books.ioasys.com.br/api/v1',
  headers: {
    'Content-Type': 'application/json',
    accept: 'application/json',
  },
});

api.interceptors.request.use(async (config) => {
  const token = await isAuthenticated();
  // eslint-disable-next-line no-param-reassign
  config.headers.authorization = `Bearer ${token}`;
  return config;
});

export default api;
