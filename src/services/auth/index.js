import { getStoredState } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['books'],
};

export const isAuthenticated = async () => {
  const stored = await getStoredState(persistConfig);
  return stored?.login?.userData?.authorization;
};
