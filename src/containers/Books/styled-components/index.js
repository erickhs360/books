import styled from 'styled-components';

export const BooksTag = styled.section`
  display: flex;
  flex-flow: row wrap;
  gap: 16px;
`;
