import React from 'react';
import PropTypes from 'prop-types';
import { BooksTag } from './styled-components';
import Book from '../../components/Book/Book';
import BookDetails from '../../components/BookDetails/BookDetails';

const Books = ({ books }) => {
  return (
    <BooksTag books={books}>
      {books?.map((book) => (
        <Book data={book} onClick={() => <BookDetails data={book} />} />
      ))}
    </BooksTag>
  );
};

Books.propTypes = {
  books: PropTypes.arrayOf({}),
};

export default Books;
