import React from 'react';
import PropTypes, { string } from 'prop-types';
import { BookDetailsTag } from './styled-components';
import Button from '../Button/Button';
import CloseIcon from '../../assets/icons/Close.svg';

const BooksDetails = ({ data, onClick }) => {
  const {
    id,
    title,
    authors,
    imageUrl,
    publisher,
    published,
    pageCount,
  } = data;
  return (
    <BookDetailsTag data={data}>
      <div className='book-detail-modal'>
        <section className='img-section'>
          <figure>
            <img src={imageUrl} height={512} width={350} alt={title} />
          </figure>
        </section>
        <section className='info-section'>
          <article className='header'>
            <figcaption>{title || 'Livro'}</figcaption>

            {authors?.map((author, index) => (
              <span key={author}>
                {author}
                {authors.length !== index + 1 ? ',' : ''}{' '}
              </span>
            ))}
          </article>
          <article className='infos'>
            <h2>INFORMAÇÕES</h2>
            <div>
              <span>Páginas</span>
              <span>{pageCount || 0} páginas</span>
            </div>
            <div>
              <span>Editora</span>
              <span>{publisher || 'desconhecida'} </span>
            </div>
            <div>
              <span>Publicação</span>
              <span>{published || '0000'} </span>
            </div>
            <div>
              <span>Idioma</span>
              <span>{pageCount || 0} </span>
            </div>
            <div>
              <span>Título Original</span>
              <span>{pageCount || 0} </span>
            </div>
            <div>
              <span>ISBN-10</span>
              <span>{pageCount || 0} </span>
            </div>
            <div>
              <span>ISBN-13</span>
              <span>{pageCount || 0} </span>
            </div>
          </article>
          <article className='review'>
            <h2>RESENHA DA EDITORA</h2>
            <p>
              The subject of “design thinking” is the rage at business schools,
              throughout corporations, and increasingly in the popular press—due
              in large part to the work of IDEO, a leading design firm, and its
              celebrated CEO, Tim Brown, who uses this book to show how the
              techniques and strategies of design belong at every level of
              business.
            </p>
          </article>
        </section>
      </div>
      <Button onlyIcon icon={<CloseIcon onClick={onClick} />} />
    </BookDetailsTag>
  );
};

export default BooksDetails;
