import styled from 'styled-components';

export const BookDetailsTag = styled.article`
  display: flex;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.4);

  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
  border-radius: 4px;
  padding-right: 40px;

  position: fixed;

  top: 0;
  bottom: 0;
  right: 0;
  left: 0;

  z-index: 999;
  * {
    z-index: 999;
  }
  .book-detail-modal {
    width: 768px;
    height: 608px;
    display: flex;
    justify-content: space-between;
    background: #ffffff;
    box-shadow: 0px 16px 80px rgba(0, 0, 0, 0.32);
    border-radius: 4px;
    padding: 48px;
    box-sizing: border-box;
  }

  Button {
    position: absolute;
    top: 16px;
    right: 16px;
  }

  figure {
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    margin: 0;
    filter: drop-shadow(0px 12px 18px rgba(0, 0, 0, 0.3));
  }

  figcaption {
    font-weight: 500;
    font-size: 28px;
    line-height: 40px;
    color: #333333;
  }

  .info-section .header span {
    font-size: 12px;
    color: #ab2680;
    font-weight: normal;
  }
  .info-section {
    margin-left: 48px;
    width: 100%;

    display: flex;
    flex-direction: column;
    justify-content: space-between;
  }

  .infos {
    width: 100%;
  }

  .infos > div {
    display: flex;
    justify-content: space-between;
  }

  .info-section .infos span:nth-child(1),
  h2 {
    color: #333333;
    font-size: 12px;
    font-weight: 500;
    display: block;
    line-height: 20px;
  }

  .info-section .infos span:nth-child(2) {
    color: #999;
    font-size: 12px;
    font-weight: 400;
    display: block;
  }

  .review p {
    font-size: 12px;
    line-height: 20px;
    color: #999999;
    max-height: 124px;
    overflow-y: auto;
    padding-right: 4px;
  }

  *::-webkit-scrollbar {
    position: absolute;
    width: 4px;
    background: grey;
  }
  *::-webkit-scrollbar-thumb {
    border-radius: 20px;
    border: 4px solid #ab2680;
  }
`;
