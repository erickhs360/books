import { render, screen } from '@testing-library/react';
import Book from './BookDetails';

describe('<Book Details>', () => {
  it('should render Book Details', () => {
    render(<Book title='Book Details' />);

    screen.getByText('Book Details');
  });

  it('should execute function on click', () => {
    const mockedFunction = jest.fn();

    render(<Book onClick={mockedFunction} title='Book Details' />);
    screen.getByText('Book Details').click();

    expect(mockedFunction).toBeCalled();
  });
});
