import React from 'react';
import BookDetails from './BookDetails';

export default {
  title: 'Components/BookDetails',
  component: BookDetails,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ height: '100vh' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <BookDetails {...args} />;

export const BookDetailsExample = Template.bind({});
BookDetailsExample.args = {
  onClick: () => console.log('Button'),
};
