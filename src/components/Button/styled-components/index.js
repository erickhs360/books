/* eslint-disable no-extra-boolean-cast */
import styled from 'styled-components';

// eslint-disable-next-line import/prefer-default-export
export const ButtonTag = styled.button`
  font-weight: 600;
  font-size: 16px;
  border-radius: 44px;
  box-sizing: border-box;
  height: ${(props) => (props.onlyIcon ? '32px' : '36px')};
  width: ${(props) => (props.onlyIcon ? '32px' : 'auto')};
  padding: ${(props) => (props.onlyIcon ? '' : '8px 20px')};
  display: flex;
  align-items: center;
  justify-content: center;
  border: ${(props) =>
    props.onlyIcon ? '1px solid rgba(51, 51, 51, 0.2)' : 'none'};
  background: ${(props) => props.background};
  color: ${(props) => props.color};
  cursor: pointer;
  gap: 8px;

  .button-content {
    display: flex;
    justify-content: space-between;
    align-items: center;
  }

  &:hover {
    opacity: 0.8;
  }

  &:disabled,
  &[disabled] {
    opacity: 0.7;
    cursor: not-allowed;
  }
`;
