import React from 'react';
import PropTypes from 'prop-types';
import { ButtonTag } from './styled-components';

const Button = ({
  background = '#fff',
  color = '#B22E6F',
  disabled,
  icon,
  onClick,
  onlyIcon,
  children,
}) => {
  return (
    <ButtonTag
      background={background}
      color={color}
      disabled={disabled}
      onClick={onClick}
      onlyIcon={onlyIcon}
      type='button'
    >
      {!onlyIcon && <>{children || 'Button'}</>}
      {icon || ''}
    </ButtonTag>
  );
};

Button.propTypes = {
  background: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.node,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  icon: PropTypes.element,
  onlyIcon: PropTypes.bool,
};

export default Button;
