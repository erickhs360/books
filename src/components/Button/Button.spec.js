import { render, screen } from '@testing-library/react';
import Button from './Button';

describe('<Button>', () => {
  it('should render button', () => {
    render(<Button>test_button</Button>);

    screen.getByText('test_button');
  });

  it('should execute function on click', () => {
    const mockedFunction = jest.fn();

    render(<Button onClick={mockedFunction}>test_button</Button>);
    screen.getByText('test_button').click();

    expect(mockedFunction).toBeCalled();
  });
});
