import React from 'react';
import CloseIcon from '../../assets/icons/Close.svg';
import Button from './Button';

export default {
  title: 'Components/Button',
  component: Button,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ padding: '3em', background: 'rgba(0, 0, 0, 0.32)' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  text: 'Primary',
};

export const Disabled = Template.bind({});
Disabled.args = {
  text: 'Disabled',
  disabled: true,
};

export const OnlyIcon = Template.bind({});
OnlyIcon.args = {
  icon: <CloseIcon />,
  onlyIcon: true,
};

export const TextAndIcon = Template.bind({});
TextAndIcon.args = {
  icon: <CloseIcon />,
  text: 'olá',
};
