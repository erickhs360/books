/* eslint-disable no-extra-boolean-cast */
import styled from 'styled-components';

export const PopupValidatorTag = styled.span`
  background: rgba(255, 255, 255, 0.4);
  height: 48px;
  box-sizing: border-box;
  display: flex;
  align-items: center;
  padding: 16px;
  width: fit-content;
  border-radius: 4px;
  font-weight: 500;
  color: #fff;
  font-size: 16px;
  line-height: 16px;
  position: absolute;
  bottom: -72px;
  svg {
    position: absolute;
    top: -8px;
  }
`;
