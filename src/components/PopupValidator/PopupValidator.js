import React from 'react';
import PropTypes from 'prop-types';
import { PopupValidatorTag } from './styled-components';
import TriangleSVG from '../../assets/icons/Triangle.svg';

const PopupValidator = ({ invalid, message }) => {
  return (
    <PopupValidatorTag invalid={invalid} message={message}>
      <TriangleSVG />
      {message}
    </PopupValidatorTag>
  );
};

PopupValidator.propTypes = {
  invalid: PropTypes.bool,
  message: PropTypes.string,
};

export default PopupValidator;
