import React from 'react';
import PopupValidator from './PopupValidator';

export default {
  title: 'Components/PopupValidator',
  component: PopupValidator,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ padding: '3em', background: '#C4557F' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <PopupValidator {...args} />;

export const HeaderWithLogo = Template.bind({});
HeaderWithLogo.args = {
  invalid: true,
  message: 'Email e/ou senha incorretos.',
};
