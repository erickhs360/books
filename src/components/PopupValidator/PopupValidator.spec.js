import { render, screen } from '@testing-library/react';
import PopupValidator from './PopupValidator';

describe('<Header>', () => {
  it('should render Header', () => {
    render(<PopupValidator invalid message='invalid message' />);

    screen.getByText('invalid message');
  });
});
