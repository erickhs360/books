import styled from 'styled-components';

export const BookTag = styled.article`
  width: 272px;
  background: #ffffff;
  box-shadow: 0px 6px 24px rgba(84, 16, 95, 0.13);
  border-radius: 4px;
  height: 160px;
  padding: 20px 16px;

  box-sizing: border-box;
  display: flex;
  justify-content: space-between;
  word-break: break-keep-all;

  &:hover {
    box-shadow: 0px 16px 80px rgba(84, 16, 95, 0.32);
    cursor: pointer;
  }

  figure {
    display: flex;
    justify-content: space-between;
    box-sizing: border-box;
    margin: 0;
    filter: drop-shadow(0px 6px 9px rgba(0, 0, 0, 0.15));
  }

  figcaption {
    font-size: 14px;
    color: #333333;
    font-weight: 500;
  }

  .info-section .header span {
    font-size: 12px;
    color: #ab2680;
    font-weight: normal;
    display: block;
  }
  .info-section {
    margin-left: 16px;
    display: flex;
    flex-flow: column;
    justify-content: space-between;
  }

  .info-section .infos span {
    color: #999;
    font-size: 12px;
    font-weight: 400;
    display: block;
  }
`;
