import { render, screen } from '@testing-library/react';
import Book from './Book';

describe('<Book>', () => {
  it('should render Book', () => {
    render(<Book title='Book' />);

    screen.getByText('Book');
  });

  it('should execute function on click', () => {
    const mockedFunction = jest.fn();

    render(<Book onClick={mockedFunction} title='Book' />);
    screen.getByText('Book').click();

    expect(mockedFunction).toBeCalled();
  });
});
