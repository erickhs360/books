import React, { useState } from 'react';
import PropTypes, { string } from 'prop-types';
import { BookTag } from './styled-components';
import BooksDetails from '../BookDetails/BookDetails';

const Book = ({ data }) => {
  const {
    id,
    title,
    authors,
    imageUrl,
    publisher,
    published,
    pageCount,
  } = data;

  const [show, setShow] = useState(false);

  const toggleBookDetail = () => {
    setShow(!show);
  };

  return (
    <>
      <BookTag id={id} data={data} onClick={toggleBookDetail}>
        <section className='img-section'>
          <figure>
            <img src={imageUrl} height={122} width={81} alt={title} />
          </figure>
        </section>
        <section className='info-section'>
          <article className='header'>
            <figcaption>{title || 'Livro'}</figcaption>

            {authors?.map((author) => (
              <span>{author}</span>
            ))}
          </article>
          <article className='infos'>
            <span>{pageCount || 0} páginas</span>
            <span>Editora {publisher || 'desconhecida'}</span>
            <span>Publicado em {published || '0000'}</span>
          </article>
        </section>
      </BookTag>
      {show && <BooksDetails data={data} onClick={toggleBookDetail} />}
    </>
  );
};

Book.propTypes = {
  id: PropTypes.string,
  title: PropTypes.string,
  authors: PropTypes.arrayOf(string),
  imageUrl: PropTypes.string,
  publisher: PropTypes.string,
  published: PropTypes.number,
  pageCount: PropTypes.number,
};

export default Book;
