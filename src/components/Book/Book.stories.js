import React from 'react';
import Book from './Book';

export default {
  title: 'Components/Book',
  component: Book,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ padding: '3em', background: 'rgba(0, 0, 0, 0.32)' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <Book {...args} />;

export const BookExample = Template.bind({});
BookExample.args = {
  onClick: () => console.log('Button'),
};
