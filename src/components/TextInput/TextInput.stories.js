import React from 'react';
import TextInput from './TextInput';
import Button from '../Button/Button';

export default {
  title: 'Components/TextInput',
  component: TextInput,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ padding: '3em', background: 'rgba(0, 0, 0, 0.32)' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <TextInput {...args} />;

export const Text = Template.bind({});
Text.args = {
  label: 'Digite algum texto',
};

export const Password = Template.bind({});
Password.args = {
  type: 'password',
  label: 'Digite sua senha',
};

export const WithButton = Template.bind({});
WithButton.args = {
  type: 'password',
  label: 'Digite sua senha',
  Btn: () => <Button text='Entrar' />,
};

export const Disabled = Template.bind({});
Disabled.args = {
  label: 'Desabilitado',
  Btn: () => <Button text='Entrar' />,
  disabled: true,
};
