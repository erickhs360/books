/* eslint-disable no-extra-boolean-cast */
import styled from 'styled-components';

export const TextInputTag = styled.div`
  background: rgba(0, 0, 0, 0.32);
  backdrop-filter: blur(2px);
  border-radius: 4px;
  width: 368px;
  height: 60px;
  box-sizing: border-box;
  padding: 8px 16px;
  display: flex;
  justify-content: space-between;
  align-items: center;

  input {
    border: 0 !important;
    font-size: 16px;
    font-weight: 500;
    line-height: 24px;
    color: #ffffff;
    outline: 0;
    background-color: transparent;
    height: 100%;
    transition: all 5s;
  }

  input::placeholder {
    color: #fff;
    caret-color: #fff;
  }

  input:focus::placeholder {
    color: #fff;
    z-index: 1;
    opacity: 0;
  }

  .input-content {
    display: flex;
    flex-flow: column;
    width: 100%;
    font-weight: 500;
  }

  .label {
    color: #ffffff;
    display: block;
    opacity: 0.5;
    font-family: sans-serif;
    font-weight: 500;
    font-size: 12px;
    line-height: 16px;
    margin-bottom: 4px;
  }

  &:hover {
    opacity: 0.95;
  }

  &:disabled,
  &[disabled] {
    opacity: 0.7;
    cursor: not-allowed;
  }

  &[disabled] > * {
    pointer-events: none;
  }
`;
