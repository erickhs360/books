import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { TextInputTag } from './styled-components';
import Button from '../Button/Button';

const TextInput = ({
  background,
  label,
  color,
  disabled,
  type = 'text',
  Btn,
  value,
  setValue,
}) => {
  const [focus, setFocus] = useState(false);

  return (
    <TextInputTag
      background={background}
      color={color}
      disabled={disabled}
      label={label}
      type={type}
      Btn={Btn}
    >
      <label className='input-content'>
        <span className='label'>{value || focus ? label : ''}</span>
        <input
          value={value}
          placeholder={label}
          onChange={(e) => setValue(e.currentTarget.value)}
          onFocus={() => setFocus(true)}
          onBlur={() => setFocus(false)}
          type={type}
        />
      </label>
      {Btn && <Btn />}
    </TextInputTag>
  );
};

TextInput.propTypes = {
  background: PropTypes.string,
  color: PropTypes.string,
  label: PropTypes.string,
  type: PropTypes.string,
  Btn: PropTypes.exact(() => Button),
  disabled: PropTypes.bool,
  value: PropTypes.string,
  setValue: PropTypes.func,
};

export default TextInput;
