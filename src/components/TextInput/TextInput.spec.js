import { render, screen } from '@testing-library/react';
import TextInput from './TextInput';

describe('<TextInput>', () => {
  it('should render TextInput', () => {
    render(<TextInput label='text_input' />);

    screen.getByPlaceholderText('text_input');
  });
});
