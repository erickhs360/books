import React from 'react';
import PropTypes from 'prop-types';
import { HeaderTag } from './styled-components';
import Button from '../Button/Button';

const Header = ({ Logo, title = '', username = '', Btn }) => {
  return (
    <HeaderTag Logo={Logo} title={title} username={username} Btn={Btn}>
      <div className='logo-container'>
        {Logo && <Logo />}
        <span className='title'>{title}</span>
      </div>
      <div className='user-container'>
        {username && (
          <span>
            Bem vindo, <strong>{username}</strong>
          </span>
        )}
        {Btn && <Btn />}
      </div>
    </HeaderTag>
  );
};

Header.propTypes = {
  Logo: PropTypes.node,
  title: PropTypes.string,
  username: PropTypes.string,
  Btn: PropTypes.exact(Button),
};

export default Header;
