import React from 'react';
import Header from './Header';
import LogoSvg from '../../assets/icons/Logo.svg';
import Button from '../Button/Button';
import LogoutIcon from '../../assets/icons/LogOut.svg';

export default {
  title: 'Components/Header',
  component: Header,
  argTypes: {
    background: { control: 'color' },
    color: { control: 'color' },
  },
  decorators: [
    (Story) => (
      <div style={{ padding: '3em', background: 'rgba(0, 0, 0, 0.32)' }}>
        <Story />
      </div>
    ),
  ],
};

const Template = (args) => <Header {...args} />;

export const HeaderWithLogo = Template.bind({});
HeaderWithLogo.args = {
  title: 'Books',
  Logo: LogoSvg,
};

export const HeaderWithLogoAndUsername = Template.bind({});

HeaderWithLogoAndUsername.args = {
  title: 'Books',
  Logo: LogoSvg,
  username: 'Erick',
  Btn: () => (
    <Button onlyIcon icon={<LogoutIcon />} onClick={() => alert('logout')} />
  ),
};
