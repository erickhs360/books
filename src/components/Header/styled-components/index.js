/* eslint-disable no-extra-boolean-cast */
import styled from 'styled-components';

export const HeaderTag = styled.header`
  background: transparent;
  display: flex;
  align-items: center;
  justify-content: space-between;
  position: absolute;
  top: 0;
  right: 0;
  left: 0;

  .logo-container {
    display: flex;
    align-items: center;
    gap: 16px;
  }

  .logo-container svg path {
    fill: ${(props) => (props.username ? '#333' : '#fff')};
  }

  .title {
    font-weight: 300;
    font-size: 28px;
    color: ${(props) => (props.username ? '#333' : '#fff')};
  }

  .user-container {
    display: flex;
    align-items: center;
    gap: 16px;
    font-size: 12px;
    font-weight: normal;
  }

  @media (max-width: 768px) {
    .user-container > span {
      display: none;
    }
  }
`;
