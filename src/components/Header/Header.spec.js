import { render, screen } from '@testing-library/react';
import Header from './Header';

describe('<Header>', () => {
  it('should render Header', () => {
    render(<Header title='Header' />);

    screen.getByText('Header');
  });
});
