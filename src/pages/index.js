import Image from 'next/image';
import { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Header from '../components/Header/Header';
import { IndexTag } from './style';
import LogoSvg from '../assets/icons/Logo.svg';
import TextInput from '../components/TextInput/TextInput';
import Button from '../components/Button/Button';
import PopupValidator from '../components/PopupValidator/PopupValidator';

import { handleLogin } from '../redux';

const Index = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const dispatch = useDispatch();
  const error = useSelector((state) => state.login.error);

  return (
    <IndexTag>
      <Image
        src='/Background_Image.png'
        layout='fill'
        objectFit='cover'
        objectPosition='center'
      />

      <form>
        <Header title='Books' Logo={() => <LogoSvg />} />
        <div className='input-container'>
          <TextInput label='Email' value={email} setValue={setEmail} />
          <TextInput
            label='Senha'
            type='password'
            value={password}
            setValue={setPassword}
            Btn={() => (
              <Button
                onClick={() => dispatch(handleLogin({ email, password }))}
              >
                Entrar
              </Button>
            )}
          />
          {error && <PopupValidator invalid message={error} />}
        </div>
      </form>
    </IndexTag>
  );
};

export default Index;
