import styled from 'styled-components';

export const IndexTag = styled.main`
  img {
    z-index: 0;
  }
  display: flex;
  align-items: center;
  width: 100%;
  height: 100vh;

  form {
    display: flex;
    flex-flow: column;
    margin-left: 115px;
    position: relative;
    height: 224px;
    justify-content: space-between;
  }

  form Header {
    margin-bottom: 50px;
  }

  .input-container {
    display: flex;
    flex-direction: column;
    gap: 16px;
    margin-top: 48px;
    position: absolute;
    bottom: 0;
  }
`;
