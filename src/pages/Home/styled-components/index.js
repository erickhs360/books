import styled from 'styled-components';

export const HomeTag = styled.main`
  img {
    z-index: 0;
    background-blend-mode: darken;
    transform: matrix(1, 0, 0, -1, 0, 0);
    position: fixed;
  }
  display: flex;
  align-items: center;
  width: 100%;

  form {
    display: flex;
    flex-flow: column;
    margin-left: 115px;
    gap: 16px;
  }

  form Header {
    margin-bottom: 32px;
  }

  .container {
    width: 70vw;
    position: relative;
    margin: auto;
    margin-top: 40px;
    height: 100vh;
  }

  .books-container {
    margin-top: 72px;
  }
`;
