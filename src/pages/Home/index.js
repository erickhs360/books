import { useSelector, useDispatch } from 'react-redux';
import Image from 'next/image';
import Router from 'next/router';
import { useEffect } from 'react';
import Header from '../../components/Header/Header';
import { HomeTag } from './styled-components';
import LogoSvg from '../../assets/icons/Logo.svg';
import Button from '../../components/Button/Button';
import LogoutIcon from '../../assets/icons/LogOut.svg';
import { isAuthenticated } from '../../services/auth/index';
import { getBooks } from '../../redux';
import Books from '../../containers/Books/Books';

const Home = () => {
  if (!isAuthenticated()) return Router.replace('/');
  const username = useSelector((state) => state?.login?.userData?.name);
  const books = useSelector((state) => state?.books?.books?.data);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getBooks());
  }, []);

  return (
    <HomeTag>
      <Image
        src='/bg-home.png'
        layout='fill'
        objectFit='cover'
        objectPosition='center'
      />
      <div className='container'>
        <Header
          title='Books'
          Logo={() => <LogoSvg />}
          username={username || 'teste'}
          Btn={() => <Button onlyIcon icon={<LogoutIcon />} />}
        />
        <div className='books-container'>
          <Books books={books} />
        </div>
      </div>
    </HomeTag>
  );
};

export default Home;
