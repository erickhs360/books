import Router from 'next/router';
import LoginTypes from './types';

const initialState = {
  loading: false,
  auth: {},
  error: '',
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case LoginTypes.HANDLE_LOGIN_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case LoginTypes.HANDLE_LOGIN_SUCCESS:
      Router.replace('/Home');
      return {
        loading: false,
        userData: action.payload,
        error: '',
      };
    case LoginTypes.HANDLE_LOGIN_FAILURE:
      return {
        loading: false,
        userData: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
