import api from '../../services/api';
import LoginTypes from './types';
import { login } from '../../services/auth';

export const handleLoginRequest = () => {
  return {
    type: LoginTypes.HANDLE_LOGIN_REQUEST,
  };
};

export const handleLoginSuccess = (auth) => {
  return {
    type: LoginTypes.HANDLE_LOGIN_SUCCESS,
    payload: auth,
  };
};

export const handleLoginFailure = (error) => {
  return {
    type: LoginTypes.HANDLE_LOGIN_FAILURE,
    payload: error,
  };
};

export const handleLogin = (data) => {
  return (dispatch) => {
    dispatch(handleLoginRequest());
    api
      .post('/auth/sign-in', data)
      .then((response) => {
        const { name, email } = response.data;
        const { authorization } = response.headers;
        const refreshToken = response.headers['refresh-token'];
        dispatch(
          handleLoginSuccess({ name, email, authorization, refreshToken })
        );
      })
      .catch((error) => {
        // error.message is the error message
        dispatch(handleLoginFailure(error?.response?.data?.errors?.message));
      });
  };
};
