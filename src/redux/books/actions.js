import api from '../../services/api';
import LoginTypes from './types';

export const getBooksRequest = (page) => {
  return {
    type: LoginTypes.GET_BOOKS_REQUEST,
    payload: page,
  };
};

export const getBooksSuccess = (data) => {
  return {
    type: LoginTypes.GET_BOOKS_SUCCESS,
    payload: data,
  };
};

export const getBooksFailure = (error) => {
  return {
    type: LoginTypes.GET_BOOKS_FAILURE,
    payload: error,
  };
};

export const getBooks = (page = 1) => {
  console.log('oi');
  return (dispatch) => {
    dispatch(getBooksRequest(page));
    api
      .get(`/books?page=${page}`)
      .then((response) => {
        // response.data is the users
        console.log(response);
        const books = response.data;
        dispatch(getBooksSuccess(books));
      })
      .catch((error) => {
        console.log(error);
        // error.message is the error message
        dispatch(getBooksFailure(error.message));
      });
  };
};
