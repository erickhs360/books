import BooksTypes from './types';

const initialState = {
  loading: false,
  data: [],
  error: '',
  pages: 0,
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case BooksTypes.GET_BOOKS_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case BooksTypes.GET_BOOKS_SUCCESS:
      return {
        loading: false,
        books: action.payload,
        error: '',
      };
    case BooksTypes.GET_BOOKS_FAILURE:
      return {
        loading: false,
        users: [],
        error: action.payload,
      };
    default:
      return state;
  }
};

export default reducer;
