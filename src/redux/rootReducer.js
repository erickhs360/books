import { combineReducers } from 'redux';
import login from './login/reducer';
import books from './books/reducer';

const rootReducer = combineReducers({
  login,
  books,
});

export default rootReducer;
